#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <pthread.h>
#include <stdbool.h>
#include <math.h>

#define BILLION  1000000000L

int NUM_TH = 1;
long n = 10000;

typedef struct arr {
    long *array;
    int size;
} arr;

typedef struct msg {
    int id;
    arr *result;
} msg;

void printArr(long *arr, int n) {
    printf("size of array = %d\n", n);
    for (int i = 0; i < n; i++) {
        printf("%ld ", arr[i]);
    }
    printf("\n");
}

bool isPrime(long number) {
    if (number <= 1 || number % 2 == 0)
        return false;
    long halfOfNumber = (long) sqrt(number);
    for (long d = 3; d < halfOfNumber; d+=2) {
        if (number % d == 0) {
            return false;
        }
    }

    return true;
}

void *computeFilterNumbers(void *inputMsg) {
    msg *myMsg = (msg *) inputMsg;
    int id = myMsg->id;

    arr *resultArrayPriv = (arr *) malloc(sizeof(arr));
    resultArrayPriv->array = (long *) malloc(sizeof(long) * n / (10*NUM_TH));

    int j = 0;
    for (long nr = id + 1; nr < n; nr += NUM_TH) {
        bool ok = true;
        for (long auxElem = nr; auxElem > 0; auxElem = auxElem / 10) {
            long digit = auxElem % 10;
            if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
                ok = false;
                break;
            }
        }
        if (ok) {
            if (isPrime(nr)) {
                resultArrayPriv->array[j++] = nr;
            }
        }
    }
    resultArrayPriv->size = j;

    myMsg->result = resultArrayPriv;
    return NULL;
}

arr *filterNumbersCreateAndJoinThreads() {
    pthread_t *threads = (pthread_t *) malloc(NUM_TH * sizeof(pthread_t));
    msg *msgs = (msg *) malloc(NUM_TH * sizeof(msg));

    for (int i = 0; i < NUM_TH; i++) {
        msgs[i].id = i;
        pthread_create(&threads[i], NULL, computeFilterNumbers, (void *) (msgs + i));
    }

    for (int i = 0; i < NUM_TH; i++)
        pthread_join(threads[i], NULL);

    int size_to_be_allocated;
    for (int i = 0; i < NUM_TH; i++) {
        size_to_be_allocated += msgs[i].result->size;
    }

    arr *resultArr = (arr *) malloc(sizeof(arr));
    resultArr->array = (long *) malloc(sizeof(long) * size_to_be_allocated);
    resultArr->size = 0;
    long *end = resultArr->array;

    for (int i = 0; i < NUM_TH; i++) {
        if (msgs[i].result->size != 0) {
            resultArr->size += msgs[i].result->size;
            memcpy(end, msgs[i].result->array, sizeof(long) * msgs[i].result->size);
            end = end + msgs[i].result->size;
        }
        free(msgs[i].result->array);
        free(msgs[i].result);
    }

    free(msgs);
    free(threads);
    return resultArr;
}

int getDigitsNumber(long nr) {
    int count = 0;
    while (nr > 0) {
        nr = nr / 10;
        count++;
    }
    return count;
}

long power(int base, int power) {
    long result = 1;
    for (int i = 0; i < power; i++) {
        result = result * base;
    }
    return result;
}

bool contains(const long *numbers, int n, long looked) {
    for (int i = 0; i < n; i++) {
        if (numbers[i] == looked)
            return true;
    }
    return false;
}

void swap(long *xp, long *yp) {
    long temp = *xp;
    *xp = *yp;
    *yp = temp;
}

void selectionSort(long *arr, int n) {
    int i, j, min_idx;
    // One by one move boundary of unsorted subarray
    for (i = 0; i < n - 1; i++) {
        // Find the minimum element in unsorted array
        min_idx = i;
        for (j = i + 1; j < n; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;
        // Swap the found minimum element with the first element
        swap(&arr[min_idx], &arr[i]);
    }
}

arr *getCircledArr(long auxElem) {
    long frontElem = 0;
    int digitsNumber = getDigitsNumber(auxElem);

    arr *circledArr = (arr *) malloc(sizeof(arr));
    circledArr->array = (long *) malloc(sizeof(long) * digitsNumber);
    circledArr->size = 0;

    circledArr->array[circledArr->size] = auxElem;
    circledArr->size++;

    for (int d = 0; d < digitsNumber - 1; d++) {
        long lastDigit = auxElem % 10;
        frontElem = lastDigit * power(10, d) + frontElem;
        auxElem = auxElem / 10;

        long newCircled = frontElem * power(10, digitsNumber - d - 1) + auxElem;
        if (!contains(circledArr->array, circledArr->size, newCircled)) {
            circledArr->array[circledArr->size] = newCircled;
            circledArr->size++;
        }
    }
    // sortam vectorul cu numere circulate
    selectionSort(circledArr->array, circledArr->size);
    return circledArr;
}

arr *cutOffNonCircularPrimes(arr *inputArr) {
    arr *circledArr;
    arr *copyArr = inputArr;

    for (int i = 0; i < inputArr->size; i++) {
        long auxElem = inputArr->array[i];
        if (!contains(copyArr->array, copyArr->size, auxElem))
            continue;

        circledArr = getCircledArr(auxElem);

        bool validated = true;
        // verificam daca numerele ciculate sunt prime adica apartin copyArray
        for (int d = 0; d < circledArr->size; d++) {
            if (!contains(copyArr->array, copyArr->size, circledArr->array[d])) {
                validated = false;
                break;
            }
        }
        // daca numerele circulate sunt prime, in afara de primul, trebuie sterse
        int circledArrI = 0;
        if (validated) {
            circledArrI = 1;
        }

        // copiem in resultArray din copyArray numerele care nu fac parte din ciculatedArray
        arr *auxArr = (arr *) malloc(sizeof(arr));
        auxArr->array = (long *) malloc(sizeof(long) * inputArr->size);
        auxArr->size = 0;

        for (int copyArrI = 0; copyArrI < copyArr->size;) {
            if (circledArrI < circledArr->size) {
                if (copyArr->array[copyArrI] == circledArr->array[circledArrI]) {
                    copyArrI++;
                    circledArrI++;
                    continue;
                }
                if (copyArr->array[copyArrI] < circledArr->array[circledArrI]) {
                    auxArr->array[auxArr->size] = copyArr->array[copyArrI];
                    auxArr->size++;
                    copyArrI++;
                } else {
                    circledArrI++;
                }
            } else {
                auxArr->array[auxArr->size] = copyArr->array[copyArrI];
                auxArr->size++;
                copyArrI++;
            }
        }
        if(copyArr != inputArr){
            free(copyArr->array);
            free(copyArr);
        }
        copyArr = auxArr;
        free(circledArr->array);
        free(circledArr);
    }
    free(inputArr->array);
    free(inputArr);
    return copyArr;
}

arr *getCircularPrimes() {
    arr *result = filterNumbersCreateAndJoinThreads();
    printf("after cut off digits: 0, 2, 4, 5, 6, 8 and non-primes:\n");
    //printArr(result->array, result->size);

    selectionSort(result->array, result->size);
    printf("after selection sort:\n");
    result = cutOffNonCircularPrimes(result);
    printf("after cut off non-circular primes numbers:\n");
    //printArr(result->array, result->size);

    result->array[result->size++] = 2;
    result->array[result->size++] = 5;
    selectionSort(result->array, result->size);
    //printf("after sort:\n");
    printArr(result->array, result->size);

    return result;
}

int main(int argc, char **argvs) {
    int num_ths[] = {1, 2, 4, 8, 16};
    long ns[] = {1000000000,
                 2000000000,
                 3000000000,
                 4000000000,
                 5000000000,
                 6000000000,
                 7000000000,
                 8000000000,
                 9000000000,
                 10000000000,
                 11000000000,
                 12000000000,
                 };

    struct timespec start, stop;
    double accum;
    for (int i = 0; i < 5; i++) {
        printf("*****\n");
        printf("num_ths = %d\n", num_ths[i]);
        NUM_TH = num_ths[i];
        for (int j = 10; j < 12; j++) {//12
            printf("####\n");
            printf("n = %ld\n", ns[j]);
            n = ns[j];

            if( clock_gettime( CLOCK_REALTIME, &start) == -1 ) {
                perror( "clock gettime" );
                exit( EXIT_FAILURE );
            }

            arr *result = getCircularPrimes();
            free(result->array);
            free(result);

            if( clock_gettime( CLOCK_REALTIME, &stop) == -1 ) {
                perror( "clock gettime" );
                exit( EXIT_FAILURE );
            }

            accum = ( stop.tv_sec - start.tv_sec )
                    + ( stop.tv_nsec - start.tv_nsec )
                      /(double) BILLION;
            printf("time = %lf\n", accum / 60.0);
        }
    }

    getchar();
    return 0;
}
