import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    private static int digitsNumber(Long nr) {
        int count = 0;
        while (nr > 0) {
            nr = nr / 10;
            count++;
        }
        return count;
    }

    private static Long power(Integer base, int power) {
        Long result = 1L;
        for (int i = 0; i < power; i++) {
            result = result * base;
        }
        return result;
    }

    private static List<Long> cutOffNonCircularPrimes(List<Long> array) {
        List<Long> copyArray = new ArrayList<>(array);
        for (Long e : array) {
            Long auxElem = e;
            if (!copyArray.contains(auxElem))
                continue;
            Long frontElem = 0L;
            int digitsNumber = digitsNumber(auxElem);
            List<Long> circulatedList = new ArrayList<>();
            circulatedList.add(auxElem);
            for (int i = 0; i < digitsNumber - 1; i++) {
                Long lastDigit = auxElem % 10;
                frontElem = lastDigit * power(10, i) + frontElem;
                auxElem = auxElem / 10;

                Long newCirculated = frontElem * power(10, digitsNumber - i - 1) + auxElem;
                if (!circulatedList.contains(newCirculated)) {
                    circulatedList.add(newCirculated);
                }
            }
            boolean validated = true;
            for (Long c : circulatedList) {
                if (!copyArray.contains(c)) {
                    validated = false;
                }
            }
            if (validated) {
                circulatedList.remove(0);
            }
            for (Long c : circulatedList) {
                copyArray.remove(c);
            }
        }
        return copyArray;
    }

    public static void main(String args[]) {
        Long n;
        int NUM_TH;
        int num_ths[] = {1, 2, 4, 8, 16};
        long ns[] = {
                1000000000,
                2000000000,
                3000000000L,
                4000000000L,
                5000000000L,
                6000000000L,
                7000000000L,
                8000000000L,
                9000000000L,
                10000000000L,
                11000000000L,
                12000000000L,
        };
        for (int i = 0; i < 5; i++) {
            System.out.println("*****\n");
            System.out.println("num_ths = " + num_ths[i] + "\n");
            NUM_TH = num_ths[i];
            for (int j = 0; j < 12; j++) {//12
                List<CircularPrimesNoEratosthenes> threads = new ArrayList<>();
                Long before = System.currentTimeMillis();
                List<Long> result = new ArrayList<>();
                System.out.println("####\n");
                System.out.println("n = " + ns[j] + "\n");
                n = ns[j];

                for (int th = 0; th < NUM_TH; th++) {
                    CircularPrimesNoEratosthenes t = new CircularPrimesNoEratosthenes(th, n, NUM_TH);
                    t.start();
                    threads.add(t);
                }
                try {
                    for (CircularPrimesNoEratosthenes t : threads)
                        t.getT().join();

                } catch (Exception e) {
                    e.fillInStackTrace();
                }

                for (CircularPrimesNoEratosthenes t : threads)
                    result.addAll(t.getResult());

                result = cutOffNonCircularPrimes(result);
                result.add(2L);
                result.add(5L);
                Collections.sort(result, Comparator.naturalOrder());

                Long after = System.currentTimeMillis();
                System.out.println("computed time: " + (after - before) / 60000.0);

                System.out.println("main: ");
                System.out.println(result);
                System.gc();
            }
        }

    }
}
