//import java.util.ArrayList;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//public class CircularPrimesInt {
//
//    private static List<Integer> getPrimeNumbersSieveOfEratosthenes(Integer count) {
//        List<Integer> result = new ArrayList<>();
//
//        List<Boolean> primes = new ArrayList<>();
//
//        for (int i = 0; i < count; i++) {
//            primes.add(true);
//        }
//        for (int p = 2; p * p < count; p++) {
//            // If prime[p] is not changed, then it is a prime
//            if (primes.get(p)) {
//                // Update all multiples of p greater than or equal to the square of it
//                // numbers which are multiple of p and are less than p^2 are already been marked.
//                for (int j = p * p; j < count; j += p)
//                    primes.set(j, false);
//            }
//        }
//        // Print all prime numbers
//        for (int i = 2; i < count; i++) {
//            if (primes.get(i))
//                result.add(i);
//        }
//
//        return result;
//    }
//
//    private static List<Integer> cutOffEvenDigitNumbers(List<Integer> array) {
//        List<Integer> result = new ArrayList<>();
//
//        for (Integer elem : array) {
//            int ok = 1;
//            Integer auxElem = elem;
//            while (auxElem > 0) {
//                int digit = auxElem % 10;
//                if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
//                    ok = 0;
//                    break;
//                }
//                auxElem = auxElem / 10;
//            }
//            if (ok == 1) {
//                result.add(elem);
//            }
//        }
//        return result;
//    }
//
//    private static int digitsNumber(Integer nr) {
//        int count = 0;
//        while (nr > 0) {
//            nr = nr / 10;
//            count++;
//        }
//        return count;
//    }
//
//    private static Integer power(Integer base, int power) {
//        Integer result = 1;
//        for (int i = 0; i < power; i++) {
//            result = result * base;
//        }
//        return result;
//    }
//
//    private static List<Integer> verifyIsCircularPrime(List<Integer> array) {
//        List<Integer> copyArray = new ArrayList<>(array);
//
//        for (Integer e : array) {
//            Integer auxElem = e;
//            if (!copyArray.contains(auxElem))
//                continue;
//            Integer frontElem = 0;
//            int digitsNumber = digitsNumber(auxElem);
//            List<Integer> circulatedList = new ArrayList<>();
//            circulatedList.add(auxElem);
//            for (int i = 0; i < digitsNumber - 1; i++) {
//                Integer lastDigit = auxElem % 10;
//                frontElem = lastDigit * power(10, i) + frontElem;
//                auxElem = auxElem / 10;
//
//                Integer newCirculated = frontElem * power(10, digitsNumber - i - 1) + auxElem;
//                if (!circulatedList.contains(newCirculated)) {
//                    circulatedList.add(newCirculated);
//                }
//            }
//            boolean validated = true;
//            for (Integer c : circulatedList) {
//                if (!copyArray.contains(c)) {
//                    validated = false;
//                }
//            }
//            if (validated) {
//                circulatedList.remove(0);
//            }
//            for (Integer c : circulatedList) {
//                copyArray.remove(c);
//            }
//        }
//        return copyArray;
//    }
//
//    private static List<Integer> getCircularPrimes(Integer n) {
//        List<Integer> result = getPrimeNumbersSieveOfEratosthenes(n);
//        System.out.println("primes:\n" + result);
//        result = cutOffEvenDigitNumbers(result);
////        System.out.println("after cut:\n" + result);
//        result = verifyIsCircularPrime(result);
////        result.add(BigInteger.valueOf(2));
////        result.add(BigInteger.valueOf(5));
////        Collections.sort(result, Comparator.naturalOrder());
////        System.out.println("after validation:\n" + result);
//        return result;
//    }
//
//    public static void main(String args[]) {
//        Integer n = 100000000;
//        Long before = System.currentTimeMillis();
//        System.out.println(getCircularPrimes(n));
//        Long after = System.currentTimeMillis();
//        System.out.println("computed time: " + TimeUnit.MILLISECONDS.toMinutes((after - before)));
//        System.out.println("computed time: " + (after - before) / 60000.0);
//    }
//}