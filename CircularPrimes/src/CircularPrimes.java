import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class CircularPrimes {

    private static List<BigInteger> getPrimeNumbersSieveOfEratosthenes(BigInteger count) {
        List<BigInteger> result = new ArrayList<>();

        Map<BigInteger, Boolean> primes = new HashMap<>();
        primes.put(BigInteger.ZERO, false);
        primes.put(BigInteger.ONE, false);

        for (BigInteger i = BigInteger.valueOf(2); i.compareTo(count) <= -1; i = i.add(BigInteger.ONE)) {
            if(i.mod(BigInteger.valueOf(100000)).equals(BigInteger.ZERO))
                System.out.println(i);
            primes.put(i, true);
        }
        for (BigInteger p = BigInteger.valueOf(2); (p.multiply(p)).compareTo(count) <= -1; p = p.add(BigInteger.ONE)) {
            // If prime[p] is not changed, then it is a prime
            if(p.mod(BigInteger.valueOf(10000)).equals(BigInteger.ZERO))
                System.out.println(p);
            if (primes.get(p)) {
                // Update all multiples of p greater than or equal to the square of it
                // numbers which are multiple of p and are less than p^2 are already been marked.
                for (BigInteger j = p.multiply(p); j.compareTo(count) <= -1; j = j.add(p))
                    primes.put(j, false);
            }
        }
        // Print all prime numbers
        for (BigInteger i = BigInteger.ONE; i.compareTo(count) <= -1; i = i.add(BigInteger.ONE)) {
            if (primes.get(i))
                result.add(i);
        }

        return result;
    }

    private static List<BigInteger> cutOffEvenDigitNumbers(List<BigInteger> array) {
        List<BigInteger> result = new ArrayList<>();

        for (BigInteger elem : array) {
            int ok = 1;
            BigInteger auxElem = elem;
            while (auxElem.compareTo(BigInteger.ZERO) >= 1) {
                BigInteger digit = auxElem.mod(BigInteger.valueOf(10));
                if (digit.compareTo(BigInteger.ZERO) == 0
                        || digit.mod(BigInteger.valueOf(2)).compareTo(BigInteger.ZERO) == 0
                        || digit.mod(BigInteger.valueOf(5)).compareTo(BigInteger.ZERO) == 0) {
                    ok = 0;
                    break;
                }
                auxElem = auxElem.divide(BigInteger.valueOf(10));
            }
            if (ok == 1) {
                result.add(elem);
            }
        }
        return result;
    }

    private static int digitsNumber(BigInteger nr) {
        int count = 0;
        while (nr.compareTo(BigInteger.ZERO) >= 1) {
            nr = nr.divide(BigInteger.valueOf(10));
            count++;
        }
        return count;
    }

    private static BigInteger power(BigInteger base, int power) {
        BigInteger result = BigInteger.ONE;
        for (int i = 0; i < power; i++) {
            result = result.multiply(base);
        }
        return result;
    }

    private static List<BigInteger> verifyIsCircularPrime(List<BigInteger> array) {
        List<BigInteger> copyArray = new ArrayList<>(array);

        for (BigInteger e : array) {
            BigInteger auxElem = e;
            if (!copyArray.contains(auxElem))
                continue;
            BigInteger frontElem = BigInteger.ZERO;
            int digitsNumber = digitsNumber(auxElem);
            List<BigInteger> circulatedList = new ArrayList<>();
            circulatedList.add(auxElem);
            for (int i = 0; i < digitsNumber - 1; i++) {
                BigInteger lastDigit = auxElem.mod(BigInteger.valueOf(10));
                frontElem = lastDigit.multiply(power(BigInteger.valueOf(10), i)).add(frontElem);
                auxElem = auxElem.divide(BigInteger.valueOf(10));

                BigInteger newCirculated = frontElem.multiply(power(BigInteger.valueOf(10), digitsNumber - i - 1)).add(auxElem);
                if (!circulatedList.contains(newCirculated)) {
                    circulatedList.add(newCirculated);
                }
            }
            boolean validated = true;
            for (BigInteger c : circulatedList) {
                if (!copyArray.contains(c)) {
                    validated = false;
                }
            }
            if (validated) {
                circulatedList.remove(0);
            }
            for (BigInteger c : circulatedList) {
                copyArray.remove(c);
            }
        }
        return copyArray;
    }

    private static List<BigInteger> getCircularPrimes(BigInteger n) {
        List<BigInteger> result = getPrimeNumbersSieveOfEratosthenes(n);
        System.out.println("primes:\n" + result);
        result = cutOffEvenDigitNumbers(result);
//        System.out.println("after cut:\n" + result);
        result = verifyIsCircularPrime(result);
//        result.add(BigInteger.valueOf(2));
//        result.add(BigInteger.valueOf(5));
//        Collections.sort(result, Comparator.naturalOrder());
//        System.out.println("after validation:\n" + result);
        return result;
    }

    public static void main(String args[]) {
        BigInteger n = BigInteger.valueOf(100000000);
        Long before = System.currentTimeMillis();
        System.out.println(getCircularPrimes(n));
        Long after = System.currentTimeMillis();
        System.out.println("computed time: " + TimeUnit.MILLISECONDS.toMinutes((after - before)));
        System.out.println("computed time: " + (after - before) / 60000.0);
    }
}