import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.sqrt;

public class CircularPrimesNoEratosthenes implements Runnable {
    private int thId;
    private Long n;
    private int noThs;
    private List<Long> result;
    private Thread t;


    public Thread getT() {
        return t;
    }

    public List<Long> getResult() {
        return result;
    }

    CircularPrimesNoEratosthenes(int thId, Long n, int noThs) {
        this.thId = thId;
        this.n = n;
        this.noThs = noThs;
    }

    private static boolean isPrime(Long number) {
        if (number <= 1 || number % 2 == 0) {
            return false;
        }
        Long halfOfNumber = (long) sqrt(number) + 1;
        for (int d = 3; d < halfOfNumber; d+=2) {
            if (number % d == 0) {
                return false;
            }
        }
        return true;
    }

    private static List<Long> cutOffNonPrimes(List<Long> numbers) {
        List<Long> result = new ArrayList<>();
        for (Long elem : numbers) {
            if (isPrime(elem)) {
                result.add(elem);
//                System.out.println(elem);
            }
        }
        return result;
    }

    private List<Long> cutOffEvenDigitNumbers() {
        List<Long> result = new ArrayList<>();

        int step = 2 * noThs;
        for (long elem = 3 + 2 * this.thId; elem < this.n; elem += step) {
            boolean ok = true;
            long auxElem = elem;
            while (auxElem > 0) {
                Long digit = auxElem % 10;
                if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
                    ok = false;
                    break;
                }
                auxElem = auxElem / 10;
            }
            if (ok) {
                if (isPrime(elem)) {
                    result.add(elem);
                }
            }
        }
        return result;
    }

    @Override
    public void run() {
        this.result = cutOffEvenDigitNumbers();
    }

    public void start() {
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }
}