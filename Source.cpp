#include <stdio.h>
#include<stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>

typedef struct arr {
	int *array;
	int size;
};
bool isPrime(int number) {
	if (number <= 1)
		return false;
	int halfOfNumber = number / 2;
	for (int d = 2; d < halfOfNumber; d++) {
		if (number % d == 0) {
			return false;
		}
	}
	return true;
}

arr* cutOffEvenDigitNumbers(int limitMin, int limitMax) {
	int n = limitMax - limitMin;
	arr* resultArray = (arr*)malloc(sizeof(arr));
	resultArray->array = (int*)malloc(sizeof(int) * n);
	int j = 0;
	for (int nr = limitMin; nr < limitMax; nr++) {
		bool ok = true;
		int auxElem = nr;
		while (auxElem > 0) {
			int digit = auxElem % 10;
			if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
				ok = false;
				break;
			}
			auxElem = auxElem / 10;
		}
		if (ok) {
			resultArray->array[j++] = nr;
		}
	}
	resultArray->size = j;
	return resultArray;
}

arr* cutOffNonPrimes(arr* inputArr){
	arr* resultArr = (arr*)malloc(sizeof(arr));
	resultArr->array = (int *)malloc(sizeof(int) * inputArr->size);
	int j = 0;
	for (int i = 0; i < inputArr->size; i++) {
		if (isPrime(inputArr->array[i])) {
			resultArr->array[j++] = inputArr->array[i];
		}
	}
	resultArr->size = j;
	return resultArr;
}

int getDigitsNumber(int nr) {
	int count = 0;
	while (nr > 0) {
		nr = nr / 10;
		count++;
	}
	return count;
}

int power(int base, int power) {
	int result = 1;
	for (int i = 0; i < power; i++) {
		result = result * base;
	}
	return result;
}

bool contains(int *numbers, int n, int looked) {
	for (int i = 0; i < n; i++) {
		if (numbers[i] == looked)
			return true;
	}
	return false;
}

void swap(int *xp, int *yp)
{
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void selectionSort(int *arr, int n)
{
	int i, j, min_idx;
	// One by one move boundary of unsorted subarray 
	for (i = 0; i < n - 1; i++)
	{
		// Find the minimum element in unsorted array 
		min_idx = i;
		for (j = i + 1; j < n; j++)
			if (arr[j] < arr[min_idx])
				min_idx = j;

		// Swap the found minimum element with the first element 
		swap(&arr[min_idx], &arr[i]);
	}
}

void printArr(int *arr, int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}

arr* getCirculatedArr(int auxElem) {	
	int frontElem = 0;
	int digitsNumber = getDigitsNumber(auxElem);

	arr* circulatedArr = (arr*)malloc(sizeof(arr));
	circulatedArr->array = (int *)malloc(sizeof(int) * digitsNumber);
	circulatedArr->size = 0;

	circulatedArr->array[circulatedArr->size] = auxElem;
	circulatedArr->size++;

	for (int d = 0; d < digitsNumber - 1; d++) {
		int lastDigit = auxElem % 10;
		frontElem = lastDigit * power(10, d) + frontElem;
		auxElem = auxElem / 10;

		int newCirculated = frontElem * power(10, digitsNumber - d - 1) + auxElem;
		if (!contains(circulatedArr->array, circulatedArr->size, newCirculated)) {
			circulatedArr->array[circulatedArr->size] = newCirculated;
			circulatedArr->size++;
		}
	}
	// sortam vectorul cu numere circulate
	selectionSort(circulatedArr->array, circulatedArr->size);
	return circulatedArr;
}

arr* cutOffNonCircularPrimes(arr* inputArr) {
	arr* circulatedArr;
	arr* copyArr = (arr*)malloc(sizeof(arr));
	copyArr->array = (int *)malloc(sizeof(int) * inputArr->size);	
	memcpy(copyArr->array, inputArr->array, inputArr->size * sizeof(int));
	copyArr->size = inputArr->size;

	for (int i = 0; i < inputArr->size; i++) {
		int auxElem = inputArr->array[i];
		if (!contains(copyArr->array, copyArr->size, auxElem))
			continue;

		circulatedArr = getCirculatedArr(auxElem);		

		bool validated = true;
		// verificam daca numerele ciculate sunt prime adica apartin copyArray
		for (int d = 0; d < circulatedArr->size; d++) {
			if (!contains(copyArr->array, copyArr->size, circulatedArr->array[d])) {
				validated = false;
				break;
			}
		}
		// daca numerele circulate sunt prime, in afara de primul trebuie sterse
		int circulatedArrI = 0;
		if (validated) {
			circulatedArrI = 1;
		}		

		// copiem in resultArray din copyArray numerele care nu fac parte din ciculatedArray 
		arr* auxArr = (arr*)malloc(sizeof(arr));
		auxArr->array = (int *)malloc(sizeof(int) * inputArr->size);
		auxArr->size = 0;

		for (int copyArrI = 0; copyArrI < copyArr->size;) {
			if (circulatedArrI < circulatedArr->size) {
				if (copyArr->array[copyArrI] == circulatedArr->array[circulatedArrI]) {
					copyArrI++; circulatedArrI++; continue;
				}
				if (copyArr->array[copyArrI] < circulatedArr->array[circulatedArrI]) {
					auxArr->array[auxArr->size] = copyArr->array[copyArrI];
					auxArr->size++;
					copyArrI++;
				}
				else {
					circulatedArrI++;
				}
			}
			else {
				auxArr->array[auxArr->size] = copyArr->array[copyArrI];
				auxArr->size++;
				copyArrI++;
			}			
		}
		copyArr = auxArr;
	}
	return copyArr;
}

arr *getCircularPrimes(int n) {
	arr* result = cutOffEvenDigitNumbers(1, n);
	printf("after cut off digits: 0, 2, 4, 5, 6, 8:\n");
	printArr(result->array, result->size);

	result = cutOffNonPrimes(result);
	printf("after cut off non-primes numbers:\n");
	printArr(result->array, result->size);

	result = cutOffNonCircularPrimes(result);
	printf("after cut off non-circular primes numbers:\n");
	printArr(result->array, result->size);

	result->array[result->size++] = 2;
	result->array[result->size++] = 5;
	selectionSort(result->array, result->size);
	printf("after sort:\n");
	printArr(result->array, result->size);

	return result;
}

int main(int argc, char** argvs) {
	int n = 1000;
	clock_t start_t, end_t, total_t;
	start_t = clock();
	getCircularPrimes(n);
	end_t = clock();
	printf("%f", total_t = (double)(end_t - start_t) / CLOCKS_PER_SEC);
	getchar();
	return 0;
}