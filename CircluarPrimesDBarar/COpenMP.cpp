#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include "omp.h"

int NUM_TH = 0;

typedef struct arr {
	int *array;
	int size;
};

void printArr(int *arr, int n) {
	printf("size of array = %d\n", n);
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}

bool isPrime(int number) {
	if (number <= 1)
		return false;
	int halfOfNumber = number / 2;
	int d;
	for (d = 2; d < halfOfNumber; d++) {
		if (number % d == 0) {
			return false;
		}
	}

	return true;
}

arr* cutOffEvenDigitNumbers(int limitMin, int limitMax) {
	int n = limitMax - limitMin;
	arr* resultArray = (arr*)malloc(sizeof(arr));
	resultArray->array = (int*)malloc(sizeof(int) * n);
	resultArray->size = 0;
	int *end = resultArray->array;

#pragma omp parallel
	{
		arr* resultArrayPriv = (arr*)malloc(sizeof(arr));
		resultArrayPriv->array = (int*)malloc(sizeof(int) * (n / NUM_TH));

		int j = 0;
		int id = omp_get_thread_num();
		for (int nr = id + 1; nr < limitMax; nr += NUM_TH) {
			bool ok = true;
			for (int auxElem = nr; auxElem > 0; auxElem = auxElem / 10) {
				int digit = auxElem % 10;
				if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
					ok = false;
					break;
				}
			}
			if (ok) {
				resultArrayPriv->array[j++] = nr;
			}
		}
		resultArrayPriv->size = j;

#pragma omp barrier
		if (resultArrayPriv->size != 0) {
#pragma omp critical
			{
				resultArray->size += resultArrayPriv->size;
				memcpy(end, resultArrayPriv->array, sizeof(int) * resultArrayPriv->size);
				end = end + resultArrayPriv->size;
			}
		}
		free(resultArrayPriv);
	}
	return resultArray;
}

arr* cutOffNonPrimes(arr* inputArr){
	arr* resultArr = (arr*)malloc(sizeof(arr));
	resultArr->array = (int *)malloc(sizeof(int) * inputArr->size);
	resultArr->size = 0;
	int *end = resultArr->array;
	
	#pragma omp parallel
	{
		arr* resultArrayPriv = (arr*)malloc(sizeof(arr));
		resultArrayPriv->array = (int*)malloc(sizeof(int) * (inputArr->size / NUM_TH));

		int j = 0;
		int id = omp_get_thread_num();
		for (int i = id; i < inputArr->size; i+=NUM_TH) {
			if (isPrime(inputArr->array[i])) {
				resultArrayPriv->array[j++] = inputArr->array[i];
			}
			if (i % 1111 == 0) {
				printf("%d ", i);
			}
		}
		resultArrayPriv->size = j;

		#pragma omp barrier
		#pragma omp critical
		{
			if (resultArrayPriv->size != 0) {	
				resultArr->size += resultArrayPriv->size;
				memcpy(end, resultArrayPriv->array, sizeof(int) * resultArrayPriv->size);
				end = end + resultArrayPriv->size;
			}
		}
		free(resultArrayPriv);
	}
	free(inputArr);
	return resultArr;
}

int getDigitsNumber(int nr) {
	int count = 0;
	while (nr > 0) {
		nr = nr / 10;
		count++;
	}
	return count;
}

int power(int base, int power) {
	int result = 1;
	for (int i = 0; i < power; i++) {
		result = result * base;
	}
	return result;
}

bool contains(int *numbers, int n, int looked) {
	for (int i = 0; i < n; i++) {
		if (numbers[i] == looked)
			return true;
	}
	return false;
}

void swap(int *xp, int *yp)
{
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void selectionSort(int *arr, int n)
{
	int i, j, min_idx;
	// One by one move boundary of unsorted subarray 
	for (i = 0; i < n - 1; i++) {
		// Find the minimum element in unsorted array 
		min_idx = i;
		for (j = i + 1; j < n; j++)
			if (arr[j] < arr[min_idx])
				min_idx = j;

		// Swap the found minimum element with the first element 
		swap(&arr[min_idx], &arr[i]);
	}
}

arr* getCircledArr(int auxElem) {	
	int frontElem = 0;
	int digitsNumber = getDigitsNumber(auxElem);

	arr* circledArr = (arr*)malloc(sizeof(arr));
	circledArr->array = (int *)malloc(sizeof(int) * digitsNumber);
	circledArr->size = 0;

	circledArr->array[circledArr->size] = auxElem;
	circledArr->size++;

	for (int d = 0; d < digitsNumber - 1; d++) {
		int lastDigit = auxElem % 10;
		frontElem = lastDigit * power(10, d) + frontElem;
		auxElem = auxElem / 10;

		int newCircled = frontElem * power(10, digitsNumber - d - 1) + auxElem;
		if (!contains(circledArr->array, circledArr->size, newCircled)) {
			circledArr->array[circledArr->size] = newCircled;
			circledArr->size++;
		}
	}
	// sortam vectorul cu numere circulate
	selectionSort(circledArr->array, circledArr->size);
	return circledArr;
}

arr* cutOffNonCircularPrimes(arr* inputArr) {
	arr* circledArr;
	arr* copyArr = inputArr;
	//arr* copyArr = (arr*)malloc(sizeof(arr));
	//copyArr->array = (int *)malloc(sizeof(int) * inputArr->size);	
	//memcpy(copyArr->array, inputArr->array, inputArr->size * sizeof(int));
	//copyArr->size = inputArr->size;

	for (int i = 0; i < inputArr->size; i++) {
		int auxElem = inputArr->array[i];
		if (!contains(copyArr->array, copyArr->size, auxElem))
			continue;

		circledArr = getCircledArr(auxElem);		

		bool validated = true;
		// verificam daca numerele ciculate sunt prime adica apartin copyArray
		for (int d = 0; d < circledArr->size; d++) {
			if (!contains(copyArr->array, copyArr->size, circledArr->array[d])) {
				validated = false;
				break;
			}
		}
		// daca numerele circulate sunt prime, in afara de primul, trebuie sterse
		int circledArrI = 0;
		if (validated) {
			circledArrI = 1;
		}		

		// copiem in resultArray din copyArray numerele care nu fac parte din ciculatedArray 
		arr* auxArr = (arr*)malloc(sizeof(arr));
		auxArr->array = (int *)malloc(sizeof(int) * inputArr->size);
		auxArr->size = 0;

		for (int copyArrI = 0; copyArrI < copyArr->size;) {
			if (circledArrI < circledArr->size) {
				if (copyArr->array[copyArrI] == circledArr->array[circledArrI]) {
					copyArrI++; circledArrI++; continue;
				}
				if (copyArr->array[copyArrI] < circledArr->array[circledArrI]) {
					auxArr->array[auxArr->size] = copyArr->array[copyArrI];
					auxArr->size++;
					copyArrI++;
				}
				else {
					circledArrI++;
				}
			}
			else {
				auxArr->array[auxArr->size] = copyArr->array[copyArrI];
				auxArr->size++;
				copyArrI++;
			}			
		}
		copyArr = auxArr;
		free(circledArr);
	}
	free(inputArr);
	return copyArr;
}

arr *getCircularPrimes(int n) {
	arr* result = cutOffEvenDigitNumbers(1, n);
	printf("after cut off digits: 0, 2, 4, 5, 6, 8:\n");
	//printArr(result->array, result->size);
	
	//selectionSort(result->array, result->size);
	result = cutOffNonPrimes(result);
	printf("after cut off non-primes numbers:\n");
	//printArr(result->array, result->size);
	
	selectionSort(result->array, result->size);
	result = cutOffNonCircularPrimes(result);
	printf("after cut off non-circular primes numbers:\n");
	printArr(result->array, result->size);

	result->array[result->size++] = 2;
	result->array[result->size++] = 5;
	selectionSort(result->array, result->size);
	//printf("after sort:\n");
	printArr(result->array, result->size);
	
	return result;
}

int main(int argc, char** argvs) {
	int num_ths[] = { 1, 2, 4, 8, 16 };
	int ns[] = { 1000000, 10000000, 20000000, 30000000, 40000000, 50000000, 60000000, 70000000, 73000000, 74000000, 75000000, 80000000 };
	int n;
	clock_t start_t, end_t;
	for (int i = 3; i < 5; i++) {
		printf("*****\n");
		printf("num_ths = %d\n", num_ths[i]);
		for (int j = 8; j < 9; j++) {//12
			printf("####\n");
			printf("n = %d\n", ns[j]);

			omp_set_num_threads(num_ths[i]);
			NUM_TH = num_ths[i];
			n = ns[j];
			
			start_t = clock();
			arr* result = getCircularPrimes(n);
			free(result);
			end_t = clock();
			printf("time = %f\n", (double)(end_t - start_t) / CLOCKS_PER_SEC / 60);
		}
	}
	
	getchar();
	return 0;
}