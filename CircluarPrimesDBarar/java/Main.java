import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    private static int digitsNumber(Integer nr) {
        int count = 0;
        while (nr > 0) {
            nr = nr / 10;
            count++;
        }
        return count;
    }

    private static Integer power(Integer base, int power) {
        Integer result = 1;
        for (int i = 0; i < power; i++) {
            result = result * base;
        }
        return result;
    }

    private static List<Integer> cutOffNonCircularPrimes(List<Integer> array) {
        List<Integer> copyArray = new ArrayList<>(array);
        for (Integer e : array) {
            Integer auxElem = e;
            if (!copyArray.contains(auxElem))
                continue;
            Integer frontElem = 0;
            int digitsNumber = digitsNumber(auxElem);
            List<Integer> circulatedList = new ArrayList<>();
            circulatedList.add(auxElem);
            for (int i = 0; i < digitsNumber - 1; i++) {
                Integer lastDigit = auxElem % 10;
                frontElem = lastDigit * power(10, i) + frontElem;
                auxElem = auxElem / 10;

                Integer newCirculated = frontElem * power(10, digitsNumber - i - 1) + auxElem;
                if (!circulatedList.contains(newCirculated)) {
                    circulatedList.add(newCirculated);
                }
            }
            boolean validated = true;
            for (Integer c : circulatedList) {
                if (!copyArray.contains(c)) {
                    validated = false;
                }
            }
            if (validated) {
                circulatedList.remove(0);
            }
            for (Integer c : circulatedList) {
                copyArray.remove(c);
            }
        }
        return copyArray;
    }

    public static void main(String args[]) {
        Long before = System.currentTimeMillis();
        Integer n = 74000000;
        List<Integer> result = new ArrayList<>();
        List<CircularPrimesNoEratosthenes> threads = new ArrayList<>();
        int numThreads = 16;

        for (int i = 0; i < numThreads; i++) {
            CircularPrimesNoEratosthenes t = new CircularPrimesNoEratosthenes(i, n, numThreads);
            t.start();
            threads.add(t);
        }
        try {
            for (CircularPrimesNoEratosthenes t : threads)
                t.getT().join();

        } catch (Exception e) {
            e.fillInStackTrace();
        }

        for (CircularPrimesNoEratosthenes t : threads)
            result.addAll(t.getResult());

        result = cutOffNonCircularPrimes(result);
        result.add(2);
        result.add(5);
        Collections.sort(result, Comparator.naturalOrder());

        Long after = System.currentTimeMillis();
        System.out.println("computed time: " + (after - before) / 60000.0);

        System.out.println("main: ");
        System.out.println(result);
    }
}
