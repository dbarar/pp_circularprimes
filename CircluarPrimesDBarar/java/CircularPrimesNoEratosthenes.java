import java.util.ArrayList;
import java.util.List;

public class CircularPrimesNoEratosthenes implements Runnable {
    private int limitMin;
    private int limitMax;
    private int step;
    private List<Integer> result;
    private Thread t;


    public Thread getT() {
        return t;
    }

    public List<Integer> getResult() {
        return result;
    }

    CircularPrimesNoEratosthenes(int limitMin, int limitMax, int step) {
        this.limitMin = limitMin;
        this.limitMax = limitMax;
        this.step = step;
    }

    private static boolean isPrime(Integer number) {
        if (number <= 1) {
            return false;
        }
        int halfOfNumber = number / 2;
        for (int d = 2; d < halfOfNumber; d++) {
            if (number % d == 0) {
                return false;
            }
        }
        return true;
    }

    private static List<Integer> cutOffNonPrimes(List<Integer> numbers) {
        List<Integer> result = new ArrayList<>();
        for (Integer elem : numbers) {
            if (isPrime(elem)) {
                result.add(elem);
//                System.out.println(elem);
            }
        }

        return result;
    }

    private List<Integer> cutOffEvenDigitNumbers() {
        List<Integer> result = new ArrayList<>();

        for (Integer elem = this.limitMin; elem < this.limitMax; elem += step) {
            boolean ok = true;
            Integer auxElem = elem;
            while (auxElem > 0) {
                int digit = auxElem % 10;
                if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
                    ok = false;
                    break;
                }
                auxElem = auxElem / 10;
            }
            if (ok) {
                result.add(elem);
            }
        }
        return result;
    }

    @Override
    public void run() {
//        Long before = System.currentTimeMillis();
        List<Integer> result = cutOffEvenDigitNumbers();
        this.result = cutOffNonPrimes(result);
//        Long after = System.currentTimeMillis();
//        System.out.println("computed time: " + (after - before) / 60000.0);

    }

    public void start() {
//        System.out.println("Starting ");
        if (t == null) {
            t = new Thread(this);
            t.start();
        }
    }
}