/********is_prime********/
is_prime(2) :- !.
is_prime(3) :- !.
is_prime(X) :-
    X > 3,
    X mod 2 =\= 0,
    N_max is ceiling(sqrt(X)),
    is_prime_(X,3,N_max).
is_prime_(X,N,N_max) :-
    (  N > N_max 
    -> true
    ;  0 =\= X mod N,
       M is N + 2,
       is_prime_(X,M,N_max)
    ).
/****is_legit:doesn't have digits 0, 2, 4, 5, 6, 8*/
is_legit(I):-
	(	I =< 0 -> true;
		Digit is I mod 10,
		0 \= Digit, !,
		2 \= Digit,
		4 \= Digit,
		5 \= Digit,
		6 \= Digit,
		8 \= Digit,
		I1 is div(I, 10),
		is_legit(I1)
	).
/****digits_number*****/
digits(X, 1) :-
	10 > X, !,
	X > 0.
digits(X, Y) :-
	A is div(X,10),
	digits(A, B),
	Y is B + 1.
	
/****circled_numbers****/
circled_numbers(Elem, R):-
	digits(Elem, DigitsNumber),
	circled_numbers_(Elem, 0, DigitsNumber, 0, R).
	
circled_numbers_(_, DigitsNumber, DigitsNumber, _, []):-!.

circled_numbers_(Elem, D, DigitsNumber, Front, [NewElem|R]):-
	LastDigit is Elem mod 10,
	Front1 is (LastDigit * (10 ** D)) + Front,
	Elem1 is div(Elem, 10),
	NewElem is (Front1 * (10 ** (DigitsNumber - D - 1))) + Elem1,
	D1 is D + 1,
	circled_numbers_(Elem1, D1, DigitsNumber, Front1, R).

/****contained_list_in_list:succeeds if all elements of L1 are contained in L2*/
contained_in(L1, L2) :- 
	maplist(contains(L2), L1).
	
contains(L, X) :- 
	member(X, L).
/****remove_list from list (FromList, WhatList, ResultList)****/
remove_list([], _, []).

remove_list([X|Tail], L2, Result):- 
	member(X, L2), !, 
	remove_list(Tail, L2, Result).
	
remove_list([X|Tail], L2, [X|Result]):- 
	remove_list(Tail, L2, Result).

/****circled_primes*****/
circled_primes(_, [], FR, FR):- !.

circled_primes(InputStatic, [E|Input], R, FR):-
	circled_numbers(E, CircledNumbers),
	sort(CircledNumbers, [H|CircledNumbers1]),
	(
		contained_in([H|CircledNumbers1], InputStatic) ->
			remove_list(Input, CircledNumbers1, Input1), 
			R1 = [E|R]
		; 	remove_list(Input, [H|CircledNumbers1], Input1),
			R1 = R
	),
	circled_primes(InputStatic, Input1, R1, FR).
/****join_threads******/
join_threads([], FR, FR) :- !.
	%write("FR = " + FR + "\n").

join_threads([Id|IdThs], R, FR) :- 
	%write("Id" + Id + "\n"),
	thread_join(Id, exited(RTh)),
	%write("RTh = " + RTh + "\n"),
	append(R, RTh, AR),
	join_threads(IdThs, AR, FR).
/****create_threads****/
create_threads(_, NoThs, NoThs, []) :- !.

create_threads(N, NoThs, IdTh, [Id|IdThs]) :-
	thread_create(worker(IdTh, N, IdTh, NoThs, [], _), Id, []),
	IdTh1 is IdTh + 1,
	create_threads(N, NoThs, IdTh1, IdThs).
	
create_threads(N, NoThs, IdThs) :-
	create_threads(N, NoThs, 0, IdThs).
/****worker***********/
worker(Id, N, I, NoThs, R, FR) :-
	I =< N,
	%afisare(Id,I),
	is_legit(I),
	is_prime(I), !,
	I1 is I + NoThs,
	worker(Id, N, I1, NoThs, [I|R], FR).

worker(Id, N, I, NoThs, R, FR) :-
	I =< N, !,
	I1 is I + NoThs,
	worker(Id, N, I1, NoThs, R, FR).
	
worker(_, _, _, _, FR, FR):- !,
	%write("finished worker" + FR + "\n"),
	thread_exit(FR).
/*****afisare****/
afisare(Id,N) :-
	format("Thread (~w): ~w\n",[Id,N]).
/****main*****/
main(N, NrThs, Minutes) :-
	get_time(Start),
	
	create_threads(N, NrThs, IdThs),
	join_threads(IdThs, [], FR),
	sort(FR, FR1),
	circled_primes(FR1, FR1, [], Rez),
	append(Rez, [2, 5], Rez1),
	sort(Rez1, Rez2),
	
	get_time(Stop),
	Seconds is Stop - Start,
	Minutes is Seconds / 60,
	write("Circled Primes = " + Rez2 + "\n").

/****run commands******
 create_threads(10, 4, IdThs), join_threads(IdThs, [], FR).
*/
