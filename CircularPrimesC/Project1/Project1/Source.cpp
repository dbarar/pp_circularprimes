#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include "omp.h"

int NUM_TH = 0;
long long n;

typedef struct arr {
	long long *array;
	int size;
} arr;

void printArr(long long *arr, int n) {
	printf("size of array = %d\n", n);
	for (int i = 0; i < n; i++) {
		printf("%lld ", arr[i]);
	}
	printf("\n");
}

bool isPrime(long long number) {
	if (number <= 1 || number % 2 == 0)
		return false;
	long long halfOfNumber = (long long) sqrt(number) + 1;
	for (long long d = 3; d < halfOfNumber; d+=2) {
		if (number % d == 0) {
			return false;
		}
	}

	return true;
}

arr* cutOffEvenDigitNumbers() {
	arr* resultArray = (arr*)malloc(sizeof(arr));
	resultArray->array = (long long*)malloc(sizeof(long long) * n / 100);
	resultArray->size = 0;
	long long *end = resultArray->array;

#pragma omp parallel
	{
		arr* resultArrayPriv = (arr*)malloc(sizeof(arr));
		resultArrayPriv->array = (long long*)malloc(sizeof(long long) * (n / (100 * NUM_TH)));

		int j = 0;
		int id = omp_get_thread_num();
		int step = 2 * NUM_TH;
		for (long long nr = 3 + 2 * id; nr < n; nr += step) {
			bool ok = true;
			for (long long auxElem = nr; auxElem > 0; auxElem = auxElem / 10) {
				long long digit = auxElem % 10;
				if (digit == 0 || digit % 2 == 0 || digit % 5 == 0) {
					ok = false;
					break;
				}
			}
			if (ok) {
				if (isPrime(nr)) {
					resultArrayPriv->array[j++] = nr;
				}
				if (nr % 1111111 == 0) {
					printf("%lld ", nr);
				}
				//resultArrayPriv->array[j++] = nr;
			}
		}
		resultArrayPriv->size = j;

#pragma omp barrier
		if (resultArrayPriv->size != 0) {
#pragma omp critical
			{
				resultArray->size += resultArrayPriv->size;
				memcpy(end, resultArrayPriv->array, sizeof(long long) * resultArrayPriv->size);
				end = end + resultArrayPriv->size;
			}
		}
		free(resultArrayPriv->array);
		free(resultArrayPriv);
	}
	return resultArray;
}

arr* cutOffNonPrimes(arr* inputArr) {
	arr* resultArr = (arr*)malloc(sizeof(arr));
	resultArr->array = (long long *)malloc(sizeof(long long) * inputArr->size);
	resultArr->size = 0;
	long long *end = resultArr->array;

#pragma omp parallel
	{
		arr* resultArrayPriv = (arr*)malloc(sizeof(arr));
		resultArrayPriv->array = (long long*)malloc(sizeof(long long) * (inputArr->size / NUM_TH));

		int j = 0;
		int id = omp_get_thread_num();
		for (int i = id; i < inputArr->size; i += NUM_TH) {
			if (isPrime(inputArr->array[i])) {
				resultArrayPriv->array[j++] = inputArr->array[i];
			}
			if (i % 1111111 == 0) {
				printf("%d ", i);
			}
		}
		resultArrayPriv->size = j;

#pragma omp barrier
#pragma omp critical
		{
			if (resultArrayPriv->size != 0) {
				resultArr->size += resultArrayPriv->size;
				memcpy(end, resultArrayPriv->array, sizeof(long long) * resultArrayPriv->size);
				end = end + resultArrayPriv->size;
			}
		}
		free(resultArrayPriv->array);
		free(resultArrayPriv);
	}
	free(inputArr->array);
	free(inputArr);
	return resultArr;
}

int getDigitsNumber(long long nr) {
	int count = 0;
	while (nr > 0) {
		nr = nr / 10;
		count++;
	}
	return count;
}

long long power(int base, int power) {
	long long result = 1;
	for (int i = 0; i < power; i++) {
		result = result * base;
	}
	return result;
}

bool contains(long long *numbers, int n, long long looked) {
	for (int i = 0; i < n; i++) {
		if (numbers[i] == looked)
			return true;
	}
	return false;
}

void swap(long long *xp, long long *yp)
{
	long long temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void selectionSort(long long *arr, int n)
{
	int i, j, min_idx;
	// One by one move boundary of unsorted subarray 
	for (i = 0; i < n - 1; i++) {
		// Find the minimum element in unsorted array 
		min_idx = i;
		for (j = i + 1; j < n; j++)
			if (arr[j] < arr[min_idx])
				min_idx = j;

		// Swap the found minimum element with the first element 
		swap(&arr[min_idx], &arr[i]);
	}
}

arr* getCircledArr(long long auxElem) {
	long long frontElem = 0;
	int digitsNumber = getDigitsNumber(auxElem);

	arr* circledArr = (arr*)malloc(sizeof(arr));
	circledArr->array = (long long *)malloc(sizeof(long long) * digitsNumber);
	circledArr->size = 0;

	circledArr->array[circledArr->size] = auxElem;
	circledArr->size++;

	for (int d = 0; d < digitsNumber - 1; d++) {
		long long lastDigit = auxElem % 10;
		frontElem = lastDigit * power(10, d) + frontElem;
		auxElem = auxElem / 10;

		long long newCircled = frontElem * power(10, digitsNumber - d - 1) + auxElem;
		if (!contains(circledArr->array, circledArr->size, newCircled)) {
			circledArr->array[circledArr->size] = newCircled;
			circledArr->size++;
		}
	}
	// sortam vectorul cu numere circulate
	selectionSort(circledArr->array, circledArr->size);
	return circledArr;
}

arr* cutOffNonCircularPrimes(arr* inputArr) {
	arr* circledArr;
	arr* copyArr = inputArr;

	for (int i = 0; i < inputArr->size; i++) {
		long long auxElem = inputArr->array[i];
		if (!contains(copyArr->array, copyArr->size, auxElem))
			continue;

		circledArr = getCircledArr(auxElem);

		bool validated = true;
		// verificam daca numerele ciculate sunt prime adica apartin copyArray
		for (int d = 0; d < circledArr->size; d++) {
			if (!contains(copyArr->array, copyArr->size, circledArr->array[d])) {
				validated = false;
				break;
			}
		}
		// daca numerele circulate sunt prime, in afara de primul, trebuie sterse
		int circledArrI = 0;
		if (validated) {
			circledArrI = 1;
		}

		// copiem in resultArray din copyArray numerele care nu fac parte din ciculatedArray 
		arr* auxArr = (arr*)malloc(sizeof(arr));
		auxArr->array = (long long *)malloc(sizeof(long long) * inputArr->size);
		auxArr->size = 0;

		for (int copyArrI = 0; copyArrI < copyArr->size;) {
			if (circledArrI < circledArr->size) {
				if (copyArr->array[copyArrI] == circledArr->array[circledArrI]) {
					copyArrI++; circledArrI++; continue;
				}
				if (copyArr->array[copyArrI] < circledArr->array[circledArrI]) {
					auxArr->array[auxArr->size] = copyArr->array[copyArrI];
					auxArr->size++;
					copyArrI++;
				}
				else {
					circledArrI++;
				}
			}
			else {
				auxArr->array[auxArr->size] = copyArr->array[copyArrI];
				auxArr->size++;
				copyArrI++;
			}
		}
		if (copyArr != inputArr) {
			free(copyArr->array);
			free(copyArr);
		}
		copyArr = auxArr;
		free(circledArr->array);
		free(circledArr);
	}
	free(inputArr->array);
	free(inputArr);
	return copyArr;
}

arr *getCircularPrimes() {
	arr* result = cutOffEvenDigitNumbers();
	printf("after cut off digits: 0, 2, 4, 5, 6, 8:\n");
	//printArr(result->array, result->size);

	//selectionSort(result->array, result->size);
	//result = cutOffNonPrimes(result);
	//printf("after cut off non-primes numbers:\n");
	//printArr(result->array, result->size);

	selectionSort(result->array, result->size);
	result = cutOffNonCircularPrimes(result);
	printf("after cut off non-circular primes numbers:\n");
	printArr(result->array, result->size);

	result->array[result->size++] = 2;
	result->array[result->size++] = 5;
	selectionSort(result->array, result->size);
	//printf("after sort:\n");
	printArr(result->array, result->size);

	return result;
}

int main(int argc, char** argvs) {
	int num_ths[] = { 1, 2, 4, 8, 16 };
	long long ns[] = { 
		1000000000L,
		2000000000L,
		3000000000L,
		4000000000L,
		5000000000L,
		6000000000L,
		7000000000L,
		8000000000L,
		9000000000L,
		10000000000L,
		11000000000L,
		12000000000L };

	clock_t start_t, end_t; 
	for (int i = 1; i < 5; i++) {
		printf("*****\n");
		NUM_TH = num_ths[i];
		printf("num_ths = %d\n", NUM_TH);
		omp_set_num_threads(NUM_TH);
		for (int j = 0; j < 8; j++) {//12
			printf("####\n");
			n = ns[j];
			printf("n = %lld\n", n);

			start_t = clock();
			arr* result = getCircularPrimes();
			free(result->array);
			free(result);
			end_t = clock();
			printf("time = %f\n", (double)(end_t - start_t) / CLOCKS_PER_SEC / 60);
		}
	}

	getchar();
	return 0;
}